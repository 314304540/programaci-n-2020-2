#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 19:24:09 2020

@author: luis
"""

class R():
    def __init__(self, valor):
        self.x = valor
    """overwritting"""
    def __add__(self, real2):
        return R(self.x + real2.x)        
    def __mul__(self, real2):
        return R(self.x * real2.x)
    def __truediv__(self, real2):
        return R(self.x / real2.x)
    def __str__(self):
        return "(" + str(self.x) + ")"
        
if __name__ == "__main__":
    r1 = R(1)
    r2 = R(2)
    r3 = r1 + r2
    r4 = r2 * r3
    r5 = r3 / r4
    print(r3)   
    print(r4)
    print(r5)
#%%
class R2(R):
    def __init__(self,valor_x="", valor_y=""):
        if valor_x == "" and valor_y == "":
            [valor_x, valor_y] = eval('[' +  input("dame dos numeros separados"
                                     "por una coma") + ']')
        super().__init__(valor_x)
        self.y = valor_y
    def __add__(self, p2):        
        return R2(self.x + p2.x, self.y + p2.y)
    def __str__(self):
        return super().__str__()[:-1] + ", " + str(self.y) + ")"
if __name__ == "__main__":    
    f = R2("uno","doce")
    g = R2([1.2, 3.4], [4.5, 6.7])
    e = R2()
    e = R2("","")
    a = R2(-1  , 1.5)
    b = R2(-2.2, 2)
    c = a + b
    d = a * b    
    print(a)
    print(b)
    print(c)
    print(d)
    print(e)
    print(g)

#%%
class R3(R2):
    def __init__(self,valor_x = 0, valor_y = 0, valor_z = 0):
        super().__init__(valor_x, valor_y)
        self.z = valor_z
    def __add__(self, p2):        
        return R3(self.x + p2.x, self.y + p2.y, self.z + p2.z)
    def __sub__(self, p2):
        return R3(self.x - p2.x, self.y - p2.y, self.z - p2.z)

    def X(self,b,c):
        pass
        
    def __str__(self):
        return super().__str__()[:-1] + ", " + str(self.z) + ")"

if __name__ == "__main__":
    p = R3(3, 4, 5)
    q = R3(30, 40, 50)
    r = p + q
    s = p - q
    t = R3()
    u = R3({1:4},(4,5,6,8),R2(3,4))
    print(u)
    print(t)
    print(r)
    print(s)
