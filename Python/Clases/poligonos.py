#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""
###poligonos.py

"""
1.- Apliquen los conocimientos adquiridos durante el semestre para el archivo rectángulos_y_cuadrados.py

2.- Implementen la clase polígonos, archivo polígonos.py, que utilice puntos en R2 para describirlos. 
Deben implementar el constructor, los métodos __str__, __repr__, perímetro, área(es opcional), número_de_lados, 
es_regular, es_paralelogramo,
 es_triángulo, es_pentágono, es_dodecaedro.

"""
import math as m
class rectangulo():
    def __init__(self, base, altura):
        self.b = base
        self.h = altura
        
    def area(self):
        r = 0
        r = self.b * self.h
        return r
    
    def perímetro(self):
        r = 2*self.b + 2*self.h
        return r
    
    def __str__(self):        
        return "base: " + str(self.b) + ", altura:" + str(self.h)
   # def __rep__(self):
        
    
class cuadrado(rectangulo):
    def __init__(self, lado):
        super().__init__(lado, lado)
    def __str__(self):
        return "lado: " + str(self.b)
    
r1 = rectangulo(10,5)
print(r1.area())
print(r1.perímetro())
c1 = cuadrado(5)
print(c1.area())
print(c1.perímetro())

print(str(r1))
print(str(c1))

#Entonces lo que buscamos hacer es con los ejemplos antes dados, 

class triangulo():
    def __init__(self,base,altura):
        self.b=base
        self.h=altura
        
    def area(self):
        r=0
        r=(self.b*self.h)/2
        return r
    
    def perimetro(self):
        r=0
        r=3*(m.sqrt(self.b*self.h))
        return r
    def __str__(self):
        return "base:" + str(self.b) + ",altura:" + str(self.h)
t1=triangulo(2,4)
print(t1.area())
print(t1.perimetro())
print(str(t1))
        
class pentagono():
    def __init__(self,apotema,lado):
        self.ap=apotema
        self.la=lado
    def area(self):
        r=0
        r=(5*la*ap)/2
    return r
    def perimetro(self):
        r=0
        r=5*la
    return r
    
    def __str__(self):
        return "lado:" + str(self.la)

p1=pentagono(5,4)
print(p1.area())
print(p1.perimetro())
print(str(p1))    
        
class dedocaedro():
    def __init__(self,apotema,lado):
        self.ap=apotema
        self.la=lado
    def area(self):
        r=0
        r=(5*la*ap)/2
    return r
    def perimetro(self):
        r=0
        r=12*(5*la)
    return r
    def __str__(self):
        return "lado:" +str(self.la)
d1=dedocaedro(5,3)
print(d1.area())
print(d1.perimetro())
print(str(d1))
    
        
        
        