#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


@author: luis
"""

class polinomios:    
    def __init__(self,valores=[]):
        self.p = {}
        if son_numeros(valores):
            if isinstance(valores, list):
                n = len(valores)
                "[-1, 0, 0, 0, 5, 10]"
                for i in range(n):
                    if valores[i] != 0:
                        self.p[i] = valores[i]
                        """
                        cuando i == 0
                        self.p[0] = valores[0]
                        cuando i == 1
                         no entra al if 
                        cuando i == 4
                         self.p[4] = valores[4]
                        """
            elif isinstance(valores, dict):
                self.p = valores
        else:
            print("-"*20 + "\n"
                  "Para 9 + 6*x + x^2\n"
                  "p1 = polinomios(lc)\n"
                  "donde: lc = [9, 6, 1]\n"
                  "p2 = polinoios(dc)\n"
                  "donde:dc = {0:9, 1:6, 2:1}\n"+
                  "-"*20)            
    " p6 = p2 + p3"    
    def __add__(self, pb):
        r = {}            
        for exponente, coeficiente in pb.p.items():
            if exponente in self.p:
                r[exponente] = coeficiente + self.p[exponente] 
            else:
                r[exponente] = coeficiente
        for exponente, coeficiente in self.p.items():                            
            if not exponente in r:
                r[exponente] = coeficiente                
        return polinomios(r)

    def __mul__(self, pb):
        r = {}
  
        
        return(r)
    def __call__(self, x):
        s = 0
        for exponente, coeficiente in self.p.items():
            s +=coeficiente * x ** exponente
        return s
    
    def __str1__(self):
        c = ""
        for exponente, coeficiente in self.p.items():
            c = c + "{}*x^{} + ".format(coeficiente,exponente)
        c = c[:-3]
        return c
    def __str__(self):
        c = ""
        for exponente in sorted(self.p.keys()):
            c = c + "{}*x^{} + ".format(self.p[exponente],exponente)
        c = c[:-3]
        return c    
    def __repr__(self):
        return(str(self.p))
""" 
Utilizando la representación de polinomios con diccionarios
en python donde pa y pb son dos diccionarios suma_polinomios 
es una función que recibe los dos diccionarios,
 pa y pb, y regresa el polinomio r que es el resultado de 
 sumar pa y pb.

La implementación de esta función se utiliza para definir
el método __add__ en el que "self" es "pa" y "pb" es "pb"
"""
def suma_polinomios(pa, pb):
    r = {}            
    for exponente, coeficiente in pb.p.items():
        if exponente in pa.p:
            r[exponente] = coeficiente + pa.p[exponente] 
        else:
            r[exponente] = coeficiente
    for exponente, coeficiente in pa.p.items():                            
        if not exponente in r:
            r[exponente] = coeficiente                
    return polinomios(r)
"p5 = suma_polinomios(p1, p3)"

""" 
El lugar adecuado para esta fucnión es el módulo utilidades,
 archivo utlidades.py, por lo que para que la clase 
 polinomios deberá importarla para que funcione en forma
 adecuada.
"""
def son_numeros(valores):
    from numbers import Number
    if isinstance(valores,(list,tuple)):
        return all(isinstance(n ,Number) for n in valores)
    elif isinstance(valores,dict):
        return all(isinstance(n ,Number) for n in valores.keys()) and all(isinstance(n ,Number) for n in valores.values())
#%%
if __name__ == "__main__":
    p1 = polinomios([-1, 0, 0, 0, 5, 10])      
    p2 = polinomios({0:9, 1:6, 2:1})
    p3 = p1 + p2
    p4 = p2 + p1
    print(str(p1))
    print(str(p2))
    print(p3)
    print(repr(p3))
    print(p4)
    d={1:1}
    p4 = polinomios("Luis")  
    p5 = polinomios()
    print(p5)
    print(str(p2))
    print(p2(1))
    print(p2(0))
    p2(0)
    f = polinomios([-1, 0, 0, 0, 5, 10])      
    g = polinomios({0:9, 1:6, 2:1})
    f(3)
    g(15)

    

#%%
if __name__ == "__main__":
    from numbers import Number
    l = [-1, 0, 0, 0, 5, 10]
    
    print([isinstance(n ,Number) for n in l])
    print(all(isinstance(n ,Number) for n in l))
    
    l1 = [-1, 5, "diez"]
    print([isinstance(n ,Number) for n in l1])
    print(all(isinstance(n ,Number) for n in l1))
"""
Estos son los métodos que tienen que implementar 
en la clase polinomios, ya estan implementados 
los métdos __init__, __add__, __str__, __repr__ 
que se explicaron en clase.

p1.es_diferecnia_de_cuadrados(self) 
    regresa True si el polinomio es de segundo grado
    y es una diferencia de cuadrados 
    False en otro caso.

p1.es_trinomio_cuadrado_perfecto(self)
    regresa True si el polinomio es de segundo grado
    y es un trinomio cuadrado perfecto 
    False en otro caso.

p1.es_binomio_con_termino_comun(self)
    regresa True si el polinomio es de segundo grado
    y es producto de binomios con un término común
    False en otro caso.

p1.grado(self)
    regresa el grado del polinomio.

p1.factoriza(self) 
    si p1 es un polinomio de grado 2 regresa su
    factorización.
    
p1.deriva(self)
    calcula la derivada de el polinomio representado
    en p1.
    
p1.integra(self)
    calcula la integral del polonomio representado
    en p1.
    
p1.__init__(self, args)  constructor: a = A(args)
    El constructor de objetos.
    Se implementó en clase.
p1.__call__(self, args)  call as function: a(args)
    Para llamar al objeto como función.
    Se implementó en clase.
    
p1.__str__(self)         pretty print: print a, str(a)
    Una representación del objeto p1 en una cadena.
    Se implementó en clase.
p1.__repr__(self)        representation: a = eval(repr(a))
    Una represntación del objeto en términos de los
    tipos de datos de python.
    Se implementó en clase.
p1.__add__(self, b)     p1+b
    La suma del objeto p1 con b usando el operador +
    p1 + b
    Se implementó en clase.
p1.__sub__(self, b)      p1-b
    La resta del objeto p1 con b usando el operador -
    p1 - b
p1.__mul__(self, b)      p1*b
    El producto del objeto p1 con b 
    usando el operador *
    p1 * b
p1.__truediv__(self, b)      p1/b
    La divisón del objeto p1 entre b 
    usando el operador /
    p1 / b

p1.__pow__(self, n)      p1**n
p1.__lt__(self, b)       p1<b
p1.__gt__(self, b)       p1>b
p1.__le__(self, b)       p1 <= b
p1.__ge__(self, b)       p1 => b
p1.__eq__(self, b)       p1 == b
p1.__ne__(self, b)       p1 != b
p1.__bool__(self)        boolean expression, as in if p1:
p1.__len__(self)         length of p1 (int): len(p1)
p1.__abs__(self)         abs(p1)
"""        