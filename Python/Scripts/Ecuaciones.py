# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 23:49:55 2020

@author: JulioRB
"""
print("""INTEGRANTES:
    ->Laguna Barrios Valeria
    ->Bustamante Torres Julio Rafael
    ->Peña Bernardino Andrea Esmeralda
    ->Rebeca Gomez Tagle Santos""")
    
#Sustitucion por Bustamante Torres Julio Rafael
def sustitucion(a, b, z1, c, d, z2):
    if a*d != b*c:
       x = ( ( z2 * b ) - ( d * z1 ) ) / ( ( b * c ) - ( d * a ) )
       y = ( ( z1 * c ) - ( a * z2 ) ) / ( ( b * c ) - ( d * a ) )
       return (x,y)
    elif a*d == b*c:
       print("No intersectan")


    
"""Sistemas de ecuaciones por sustitucion:
Primero busque en internet como se conseguia el metodo de sustitucion, que basicamente consiste en despejar una de los ecuaciones y con eso despejar otra incognita
Visualize dos ecuaciones de la forma ax + by = z1...1 y
 cx + dy = z2...2
De las cual primero despeje y de la 1
Dando y = (z1 -ax)*b
Sustitui este valor de y en 2 y termino dandome el valor de x como
     x = ( z1 - ( ( d / b ) * z2 ) ) / (c - ( ( d * a ) / b ) )
De lo cual despeje ( por calculadora de despejes en internet como

     x = ((z2*b) - (d*z1)) / ((b*c) - (d*a))
Tomando este valor repiti pero en la formula 1 y y terminio dandome
     y = ( z1 - (a * ( ( z2 - ( (d / b ) * z1) ) / ( c - ( ( d * a ) / b ) ) ) ) ) / b

Repito y me da
     y = ( ( ( z1 * c )-( a * z2 ) ) ) / ( ( b * c ) - ( d * a ) )
Y con esto saco dos conclusiones
B*C tiene que ser distinto de D*A para que funcione sin problemas
y el punto de interseccion es P(x,y)"""


def cramer(a, b, c, d, e, f):
    """Se quiere dar solución al sistema de ecuaciones
    ax + by = c
    dx + ey = f
    donde
    a -> es el coeficiente de la x de la primera ecuación
    b -> es el coeficiente de la y de la primera ecuación
    c -> es el coeficiente independiente de la primera ecuación
    d -> es el coeficiente de la x de la segunda ecuación
    e -> es el coeficiente de la y de la segunda ecuación
    f -> es el coeficiente independiente de la segunda ecuación
    FUNCIÓN HECHA POR: LAGUNA BARRIOS VALERIA
    """
    u = a*e - b*d
    v = c*e - b*f
    w = a*f - c*d
    """
    u el el determinante de la matriz |a b|
                                      |d e|
    v es el determinante de la matriz |c b|
                                      |f e|
    w es el determinante de la matriz |a c|
                                      |d f|
    """
    x = v / u #x es la incognita que buscamos como primera entrada
    y = w / u #y es la incognita que buscamos como segunda entrada
    return (x,y)
def igualacion(a,b,c,z,d,e,f,g):
    """
    Definimos la función igualación, con parametros a, b, c, z, d, e, f, g
    que pertenecen a las funciones:
        ax + by + c = z 
        dx + ey + f = g
    nuestra función te la intersección de ambas rectas :)
    FUNCIÓN HECHA POR: PEÑA BERNARDINO ANDREA ESMERALDA
    """
    #PROCEDIMIENTO
    #De ambas ecuaciones despejamos X e igualamos, lo que nos da por
    #resultado (z - (b*y) - c) / a = (g - (e*y) - f) / d
    #Despues depejamos Y de la ecuacion anterior y obtemos:
    # Y = ((a*g)-(a*f)-(d*z)+(c*d))/((a*e)-(d*b))
    #y con Y, sustituimos en cualquier despeje, en este caso el primero
    # X =(z-(b*y)-c)/a
    y = ((a*g)-(a*f)-(d*z)+(c*d))/((a*e)-(d*b))
    x = (z-(b*y)-c)/a
    return (x,y)
def despeje(a,b,c,d,e,f):
    x=(c-f)/(b-e)
    y=(c-b*x)/a
    return x,y
i=0
print("Para accesar a despeje tiene que escribir 0 o si no, cualquier otra opcion de resolver el problema ponga un numero entero")
i=int(input("Si quieres ocupar despeje por Rebeca en escribe 0 si no pon 1\n"))
if i == 0:
    print("Bienvenido al resolvedor de ecuaciones")
    print("En este programa, las ecuaciones tienen la forma")
    print("(a  b)=(c)")
    print("(d  e)=(f)")
    print("Ingrese los valores de los parámetros para resolver la ecuación:")

    '''si a=d, entonces (b-e)x=c-f => x=(c-f)/(b-e)
    #Luego, ay+b(c-f)/(b-e)=c => y=(c-bx)/a
    #si a!=d, entonces a*(dy+ex)=a*f ady+aex=af
    #Por otra parte, da+db=dc'''
    
    a=float(input("Valor de a: "))
    b=float(input("Valor de b: "))
    c=float(input("Valor de c: "))
    d=float(input("Valor de d: "))
    e=float(input("Valor de e: "))
    f=float(input("Valor de f: "))
    x=(c-f)/(b-e)
    y=(c-b*x)/a

    """
    En este metodo buscamos hacer cero una variable, por lo que queremos 
    sumar o restar para eliminar la x_i, primero hay que comparar si x_1 
    son iguales porque de ser asi entonces  podemos restar y despejar c/b=x_2
    """

        
    if a==d and b!=e:
        r1=x
        r2=y
        print('En tu sistema lineal, y es igual a',r1,'x es igual a',r2)
    elif a==d and b==e:
        print('El sistema lineal tiene renglones linealmente dependientes, por lo que no se puede resolver')

    elif a!=0 and d!=0:
        a_0=a
        d_0=d
        a=a*d_0
        b=b*d_0
        c=c*d_0
        d=d*a_0
        e=e*a_0
        f=f*a_0
        r1,r2=despeje(a,b,c,d,e,f)
        print('En tu sistema lineal, y es igual a',r1,',x es igual a',r2)
    else:
        print('Elige otro sistema de ecuaciones')
elif i !=0:
    print("Ponga sustitucion(a, b, z1, c, d, z2) por Julio o cramer(a, b, c, d, e, f) por Vale o igualacion(a,b,c,z,d,e,f,g) por Andrea")
    print("Igualacion esta puesto de la forma ax + by + c = z,  dx + ey + f = g")
    print("Sustitucion ax + by = z1, cx + dy = z2")
    print("Cramer ax + by = c,   dx + ey = f")
    print("Puede ocupar reduccion todavia de la forma reduccion(a,b,c,d,e,f) forma : ax + by + c = z,  dx + ey + f = g")
else:
    print("Error no pusiste un numero")
    
def reduccion(a,b,c,d,e,f):
    x=(c-f)/(b-e)
    y=(c-b*x)/a    
    if a==d and b!=e:
        r1=x
        r2=y
        print('En tu sistema lineal, y es igual a',r1,'x es igual a',r2)
    elif a==d and b==e:
        print('El sistema lineal tiene renglones linealmente dependientes, por lo que no se puede resolver')

    elif a!=0 and d!=0:
        a_0=a
        d_0=d
        a=a*d_0
        b=b*d_0
        c=c*d_0
        d=d*a_0
        e=e*a_0
        f=f*a_0
        r1,r2=despeje(a,b,c,d,e,f)
        print('En tu sistema lineal, y es igual a',r1,',x es igual a',r2)
    else:
        print('Elige otro sistema de ecuaciones')