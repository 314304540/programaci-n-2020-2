#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#
#
from math import sqrt
def seno(alpha):    
    """
 aqui se sigue el procedimiento se sin(alpha)=2sin(alpha)*sqrt(1-sen(alpha)**2)
 
    Datos de entrada
    alpha
    Datos de salida
    resultado
    """
    b=alpha/2**5
    resultado=(32*b)*(sqrt(1-(32*b)**2))
    
    '''
    Aquí el código que calcula una aproximación al
    seno de x basada en el documento Seno.pdf
    '''
    return(resultado) 
    
if __name__ == "__main__":
    seno(.5)
    print("Este bloque se ejecuta si el programa \
es llamado desde IDLE, la variable __name__ tiene \
almacenada la cadena '__main__' ")
    print(__name__)
else:
    print("Si el archivo se utiliza como modulo,\
 es decir se importa, la variable __name__ contiene\
el nombre nombre del archivo")
    print(__name__)
