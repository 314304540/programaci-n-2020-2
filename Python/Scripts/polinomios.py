#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 12:10:34 2020

@author: macbookpro
"""

#representacion de polinomios
#f(x)=1+2x^100
pl=[0]*101;pl[0];pl[99]=2
pd={0:1,100:2}
pll=[[0,1],[100,2]]
plt=[(0,1),(100,2)]

#f(5)
#1*(5)^0+0*5^2+...+0*5^99+2*5^100
n=len(pl)
suma=0
x=2
for i in range(n):
    suma= suma + pl[i]*x*i

print(suma)

s=0
for potencia in range(n):
    s+= pl[potencia]*x**potencia

print(suma)
print(s)
    