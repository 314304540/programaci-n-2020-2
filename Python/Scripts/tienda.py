#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 13:36:00 2020

@author: macbookpro
"""

#tienda.py

caja_chica=1000
inventario={
        1:{"nombre":"coca",
           "precio":15.00,
           "cantidad":1000},
         2:{"nombre":"sabritas",
           "precio":12.00,
           "cantidad":600},
            {"nombre":"galletas maria",
         3:{"precio":10.00,
           "cantidad":300}
         }
formato_venta_compra = {
        "productos" : {
                1:5,
                2:10,
                3:4
                },
                "total":235, #aqui por ejemplo va cantidad*importe
                "tipo":"compra"
                }
#El primer ejercicio consiste en definir una funcion que muestre el inventario
#En la pantalla, en formato "Bonito".
    
#El segundo ejercicio a realizar es desarrollar una funcion dado que si
#le pasamos un diccionario con el formato de compra-venta modifique
#la caja chica y la cantidad de productos dentro del inventario.