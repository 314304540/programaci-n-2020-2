#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: macbookpro
"""

import random
n=5
m=27
def adivinaA(n,m):
    cont=0
    a=random.randint(n,m)
    print(a)
    while True:
        cont +=1
        num= int(int("ingresa un numero"))
        if num== a:
            print("correcto!",cont)
            break
        elif num !=a:
            print("intenta nuevamente")   

adivinaA(n,m)

def adivinaB(n,m):
    a=random.randint(n,m)
    cont1=0
    cont2=0
    print(a)
    while True:
        num2=int(input("ingresa un numero entero"))
        if num2 < n:
            cont1+=1
        elif num2> m:
            cont2 += 1
        if num2 == a:
            print("bien hecho, lo has adivinado")
            print("los valores estan antes del intervalo",cont1)
            print("los valores estan fuera del intervalo",cont2)
            break
        elif num2 != a:
            print("intentar de nuevo")
adivinaB(n,m)

#adivinaC(n,m)

def adivinaD(n,m):
    a = random.randint(n,m)
    cont1 = 0
    cont2 = 0
    #print(a)
    while True:
        num = int(input("Introduce un número"))
        if num == a:
            print("Correcto, %i intentos fuera y %i intentos dentro" %(cont2, cont1))
            break
        elif (n < num < a) or (m < num < n):
            cont1 += 1
            print("Está dentro, inténtalo otra vez")
        else:
            cont2 += 1
            print("Está fuera, inténtalo otra vez")
            
#adivinaD(n,m)
            
def adivinaE(n,m):
    cont = 0
    a = random.randint(n,m)
    cont1 = 0
    cont2 = 0
    cont = 0
    #print(a)
    while True:
        cont += 1
        num = int(input("Introduce un número"))
        if num == a:
            print("Correcto, %i intentos mayores y %i intentos menores" %(cont2, cont1))
            break
        elif num < a:
            cont1 += 1
            print("El número que buscas es mayor, inténtalo otra vez")
        else:
            cont2 += 1
            print("El número que buscas es menor, inténtalo otra vez")

#adivinaE(n,m)

def adivinaF(n,m):
    cont1 = 0
    cont2 = 0
    lista= []
    a = random.randint(n,m)
    #print(a)
    while True:
        num = int(input("Introduce un número"))
        if num  == a:
            print("Correcto, %i intentos mayores, %i intentos menores" %(cont1,cont2))
            print("Los intentos fueron: ")
            print(lista)
            break
        elif num < a:
            print("Incorrecto, el número que buscas es mayor")
            cont2 += 1
            lista.append(num)
        elif num > a:
            print("Incorrecto, el número que nuscas es menor")
            cont1 += 1
            lista.append(num)

#adivinaF(n,m)

def adivina():
    a = random.randint(1,6)
    if a == 1:
        adivinaA(n,m)
    if a == 2:
        adivinaB(n,m)
    if a == 3:
        adivinaC(n,m)
    if a == 4:
        adivinaD(n,m)
    if a == 5:
        adivinaE(n,m)
    if a == 6:
        adivinaF(n,m)

#adivina()
