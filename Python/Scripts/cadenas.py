#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: macbookpro
"""

cadena = "Hola"
cadena2 = "oir a dario"
cadena3 = "anita lava la tina"
cadena4 = "estano" 
cadena5 = "asdedsa"
#6. Función que regresa el número de vocales almacenadas en cadena.
def contarVocales(cadena):
    cont = 0
    for i in cadena:
        if i in "aeiouAEIOUáéíóúüÜÁÉÍÓÚ":
            cont = cont + 1
    return cont
print("El numero de vocales es:", contarVocales(cadena))
contarVocales(cadena)
print()

#7.-Función que regresa el número de consonantes almacenadas en cadena.
def contarConsonantes(cadena):
    cont = 0
    for i in cadena:
        if i in "bcdfghijklmnñpqrstvwxyzBCDFGHIJKLMNÑPQRSTUVWXYZ":
            cont = cont + 1
    return cont
print("El numero de consonantes es:", contarConsonantes(cadena))
contarConsonantes(cadena)
print()

#8.- Función que regresa el número de vocales acentuadas almacenadas en cadena.
def contarAcentos(cadena):
    cont = 0
    for i in cadena:
        if i in "áéíóúÁÉÍÓÚ":
            cont = cont + 1
    return cont
print("El numero de vocales acentuadas es:", contarAcentos(cadena))
contarAcentos(cadena)
print()

#9.- Función que regresa el número de mayúsculas almacenadas en cadena.
def contarMayusculas(cadena):
    cont = 0
    for i in cadena:
        if i in "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚÜ":
            cont = cont + 1
    return cont
print("El numero de mayusculas es:", contarMayusculas(cadena))
contarMayusculas(cadena)
print()

#10.- Función que regresa el número de minusculas almacenadas en cadena
def contarMinusculas(cadena):
    cont = 0
    for i in cadena:
        if i in "abcdefghijklmnopqrstuvwxyzñáéíóúü":
            cont = cont + 1
    return cont
print("El numero de minusculas es:", contarMinusculas(cadena))
contarMinusculas(cadena)
print()

#11. Funcion que regresa True si cadena contiene un palíndromo y False si no lo es.
def palindromo(cadena):
    cadenatmp = cadena.replace("","")
    i = 0
    j = len(cadena)-1
    while(i <= j):
        if(cadenatmp[i] != cadenatmp[j]):
            return False
        i += 1
        j -= 1
    return True

print(palindromo(cadena2))
print(palindromo(cadena3))
print(palindromo(cadena4))
print(palindromo(cadena5))    