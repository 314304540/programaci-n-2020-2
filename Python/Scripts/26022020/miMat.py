#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 13:30:13 2020

@author: macbookpro
"""
def miRaiz(a):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez mas parecidos.
    Se considera una áproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    e = 0.001
    b = 1
    h = a
    while abs(b - h)>e:
        b = (b + h)/2
        h = a/b
    return b

def abs1 (x):
    """
    Primera version de valor absoluto 
    """
    if x == 0:
        return x
    elif x < 0:
        x = -1*x
        return x
    elif x>0:
        return x
    print(abs1(-1024))
    


def sigULAM(n):
    if n%2==0:
        return n/2
    else:
        return 3n + 1
