#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 18:32:14 2020

@author: macbookpro
"""
#25032020
"""
(Esta en 25032020)

(1) En una determinada empresa, sus empleados son evaluados al final de 
       cada año. Los puntos que pueden obtener en la evaluacion comienzan en 
       0.0 y pueden ir aumentando, traduciendose en mejores beneficios. Los
       puntos que pueden conseguir los empleados pueden ser 0.0, 0.4 o 0.6, 
       pero no valores intermedios entre las cifras mencionadas.
       A coninuacion se muestra una tabla con los niveles correspondientes a 
       cada puntuacion. La cantidad de dinero conseguida en cada nivel es de 
       $2,400 multiplicada por la puntuacion del nivel.
       
       |Nivel       |Puntuacion|
       |Inaceptable |0.0       |
       |Aceptable   |0.4       |
       |Meritorio   |0.6       |
        
        Escribir un programa que lea la puntuacion del usuario e indique su 
        nivel de rendimiento, asi como la cantidad de dinero que recibira el 
        usuario.
"""
puntuacion=float(input("Ingresa la puntuacion del empleado"))
if puntuacion ==0 :
    print("el nivel del empleado es inaceptable")
elif puntuacion <= 0.4:
    print("el nivel del empleado es aceptable")
else:
    print("el nivel del empleado es meritorio")
    