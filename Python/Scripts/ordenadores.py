#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""
#ordenadores.py
def espera():
    return(input("Presiona enter para continuar"))
    
print("Tipo Booleano")
t = True
f = False
print(type(t))
print(type(f))
a = 85
b = 25
valorLogico1 = a > b
valorLogico2 = a < b
valorLogico3 = a == b 
valorLogico4 = a != b 
print("a = {} b = {}".format(a, b))
print("a > b", valorLogico1)
print("a < b", valorLogico2)
print("a == b", valorLogico3)
print("a != b", valorLogico4)

espera()

print("Valores Booleanos")
cadena = "Una cadena"
x = 5.1
n = 15
l = [cadena, x ,n]
cadenaVacia = ""
y = 0.0
m = 0
lVacia = []

print(bool(cadena))
print(bool(x))
print(bool(n))
print(bool(l))
print(bool(cadenaVacia))
print(bool(y))
print(bool(m))
print(bool(lVacia))

entrada = input("Presiona enter para continuar")

print("Operadores lógicos o booleanos")
x = 0
y = 10

print (x or y) 
print (x and y) 
print (not y)

entrada = input("Presiona enter para continuar")
print("El ejercicio con while y listas")
print("Este ejercicio utiliza directamente el valor"
      "quere regresa l.pop() para utilizarlo como"
      "dato de entrada de lv.append()")
x = 5.1
n = 15
l = [cadena, x ,n]
l.append(-85)
lv = []
while bool(l):
    lv.append(l.pop())
print(l)
print(lv)

entrada = input("Presiona enter para continuar")
print("Versión almacenando el valor para"
      "mostrarlo después")
x = 5.1
n = 15
l = [cadena, x ,n]
l.append(-85)
lv = []
while bool(l):
    valor = l.pop()
    lv.append(valor)
print(l)
print(lv)

entrada = input("Presiona enter para continuar")
print("Otras versiónes sin la función bool pero"
      "con el mismo efecto")
x = 5.1
n = 15
l = [cadena, x ,n]
l.append(-85)
lv = []
while l:
    lv.append(l.pop())
print(l)
print(lv)

entrada = input("Presiona enter para continuar")
print("Versión almacenando el valor para"
      "mostrarlo después")
x = 5.1
n = 15
l = [cadena, x ,n]
l.append(-85)
lv = []
while l:
    valor = l.pop()
    lv.append(valor)
print(l)
print(lv)

entrada = input("Presiona enter para continuar")
print("El ejercicio de fin de clase")

print("-"*50)
n=100
while bool(n):
    print(n, end=", ")
    n = n-1

print("-"*50)
n=100
while bool(n):
    print(n, end=", ")
    n -= 1
    
print("-"*50)
n=100
while n:
    print(n, end=", ")
    n = n-1

print("-"*50)
n=100
while n:
    print(n, end=", ")
    n -= 1

