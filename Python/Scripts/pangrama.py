#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""
import string 
  
def pangrama(str): 
    alfabeto = "abcdefghijklmnopqrstuvwxyz"
    for char in alfabeto: 
        if char not in str.lower(): 
            return False
  
    return True
      
# Driver code 
cadena = input("ingresa una cadena")
if(pangrama(cadena) == True): 
    print("Es pangrama") 
else: 
    print("No es pangrama") 