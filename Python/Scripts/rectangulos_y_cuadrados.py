#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""
#######rectangulos_y_cuadrados.py

class rectangulo():
    def __init__(self, base, altura):
        self.b = base
        self.h = altura
        
    def area(self):
        r = 0
        r = self.b * self.h
        return r
    
    def perímetro(self):
        r = 2*self.b + 2*self.h
        return r
    
    def __str__(self):        
        return "base: " + str(self.b) + ", altura:" + str(self.h)
    
class cuadrado(rectangulo):
    def __init__(self, lado):
        super().__init__(lado, lado)
    def __str__(self):
        return "lado: " + str(self.b)
    
r1 = rectangulo(10,5)
print(r1.area())
print(r1.perímetro())
c1 = cuadrado(5)
print(c1.area())
print(c1.perímetro())

print(str(r1))
print(str(c1))
