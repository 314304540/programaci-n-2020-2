#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 19:32:33 2020

@author: luis
"""
"""
El archivo procesar_archivo.py contiene el código para leer el archivo  y
 procesar la información que contiene el archivo de texto Programación2020-05-29-11_06.sbv 
 con los datos relativos a la fruta, color, lugar de nacimiento, bachillerato, comida favorita, 
 comida no favorita, carrera, alcaldía/municipio donde se encontraban ese día, hora de despertar para ir a la facultad, 
 hora de despertar para las clases en tiempo de contingencia.

Después de comentar el código, hay que resolver algunas preguntas:
¿Cuántos alumnos contestaron las 10 preguntas?
¿Cual es la fruta que más gusta ?
¿Cual es el color que más gusta ?
¿Cual es el alimento que más gusta?
¿Cual es el alimento que menos gusta?
¿Cuantos alumnos se levantan antes de las 7 de la mañana?
¿Cuantos alumnos se levantan después de las 7 de la mañana y antes de las 11:00 de la mañana ?

Para responder estas preguntas deben utilizar los elementos vistos durante la clase, y algunos otros que investiguen y les sean útiles.
"""
nombre_archivo ='/Users/macbookpro/Documents/programaci-n-2020-2-/Python/Scripts/Programación2020-05-29-11_06.sbv'
with open(nombre_archivo) as archivo:
    mensaje = []
    todos = []
    for linea in archivo:
        if linea != '\n':
            mensaje.append(linea.rstrip())
        else:
            todos.append(mensaje)            
            mensaje = []
    datos = {}    
    for mensaje in todos:
        [nombre, *texto] = mensaje[1].split(":")
        if not nombre in datos:
            datos[nombre] = []
        datos[nombre].append(" ".join(texto))
        
for alumno in datos:
        print("_"*30)
        print(datos[alumno])

print("="*20)
for alumno in datos:
    if len(datos[alumno])== 10:
        print(datos[alumno][9])
      
#%%

nombre_archivo ='/Users/macbookpro/Documents/programaci-n-2020-2-/Python/Scripts/Programación2020-05-29-11_06.sbv'
"""
Algunas extenciones de arhivos de texto plano:
    sbv, srt, csv, xml, py, c, 
    cpp, java, ini, bat, js, 
    html, css, tex, bat
"""
with open(nombre_archivo) as f:
        for line in f:
            print(line, end = "")

#%%
"""
índice, minuto, dato, hora
 , 10, nombre, 3:17.353
0, 15, fruta, 8:34.380
1, 20, color, 13:28.206
2, 25, Lugar de nacimiento, 18:46.275
3, 30, bachillerato, 24:43.692
4, 35, platillo favorito, 28:51.877
5, 40, platillo no favorito, 35:15.960
6, 45, carrera, 39:45.466
7, 50, alcaldía/municípo, 45:12.589
8, 55, hora despertar para ir a la facultad, 50:37.852
9, 60, hora despertar contingencia, 52:49.780
"""
#Para buscar en el texto haré uso de expresiones regulares, me ayude investigando en este blog:https://rico-schmidt.name/pymotw-3/re/index.html
# y de aquí también https://robologs.net/2019/05/05/como-utilizar-expresiones-regulares-regex-en-python/
#la informacion que se obtiene a priori de compilar las lineas del código de la 26 a la 51:
""" A SIMPLE VISTA SE PUEDE OBSERBVAR QUE:
LAS FRUTAS QUE MAS GUSTAN SON O MANGO O NARANJA
EL COLOR QUE MAS GUSTA ES VERDE O NEGRO
EL ALIMENTO QUE MAS GUSTA ES TACOS O ENCHILADAS
EL ALIMENTO QUE MENOS GUSTA ES HIGADO
EL NUMERO DE ALUMNOS QUE DESPIERTA ANTES DE LAS 7 ES
EL NUMERO DE ALUMNOS QUE DESPIERTAN ENTRE LAS 7 Y LAS 11 ES
"""
#Primero importamos regex
import re
#¿Cuántos alumnos contestaron las 10 preguntas?
#de esta linea podemos observar que solo 9 alumnos contestaron las 10 preguntas
for alumno in datos:
    if len(datos[alumno])== 10:
        print(datos[alumno])
#esto nos arroja 9 resultados

#¿Cual es la fruta que más gusta ?
#esto se encuentra en alumno[0]
patron_fruta="naranja"
x=re.fullmatch(patron_fruta,alumno)
print(x)
#¿Cual es el color que más gusta ?
patron_color="[verde],[Verde]|[negro],[Negro]"
y=re.match(patron_color,alumno)
#¿Cual es el alimento que más gusta?
patron_alimento="[tacos],[Taquitos],[Tacos],[taquitos]|[enchiladas],[Enchiladas]"
z=re.fullmatch(patron_alimento,alumno)
print(z)
#¿Cual es el alimento que menos gusta?
patron_alimento_que_menos_gusta="higado|Higado"
a=re.findall(patron_alimento_que_menos_gusta,alumno)
print(a)

#¿Cuantos alumnos se levantan antes de las 7 de la mañana?
patron_horario="[0-7]"
b=re.findall(patron_horario,alumno)
print(b)
#¿Cuantos alumnos se levantan después de las 7 de la mañana y antes de las 11:00 de la mañana ?
patron_horario_2="[7-11]"
c=re.findall(patron_horario_2,alumno)
print(c)
