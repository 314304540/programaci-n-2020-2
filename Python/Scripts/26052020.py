#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 13:17:27 2020

@author: macbookpro
"""

#260520.py

#Sintaxis [[valor de salida] for i in onjeto iterable if (condicionales)]

#elevar al cuadrado los elementos de una lista
lsl=[5,10,15,20,25]
res=[]

for i in lsl:
    res.append(i**2)
print(res)

#comprension de listas elevar al cuadrado los elementos de una lista
res=[i**2 for i in [5,10,15,20,25]]
print(res)

#elevar solo los numeros pares al cuadrado

ls2=[1,2,3,4,5,6,7]
res=[]
for i in ls2:
    if i%2==0:
        res.append(i**2)
print(res)

#comprension de listas oara elevar solo los numeros pares al cuadrado
res=[i**2 for i in ls2 if i%2==0]
print(res)

#en la ls2 elevar al cuadrado si el numero es par, de lo contrario al cubo

res=[]
for i in ls2:
    if i%2==0:
        res.append(i**2)
    else:
        res.append(i**3)
print(res)

#utilizando la comprension de listas
res=[i**2 if i%2==0 else i**3 for i in ls2]
print(res)

#para cada número en ls2 obten el nuumero y su posicion en ls1
#como una lista de tuplas METODO INDEX

ls1=[9,3,6,1,5,0,8,2,4,7] 
ls2=[6,4,6,1,2,2]

