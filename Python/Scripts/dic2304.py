#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:15:44 2020

@author: macbookpro
"""

"""
1) Implementa un programa que almacene un diccionario 
con los créditos de las materias de un semestre la key 
tiene que ser el nombre de la materia y el valor asignado
 el número de créditos, después se debe mostrar el siguiente
 mensaje "(Nombre de la materia) tiene (Número de créditos) 
 ". #Hint: Puede servir el método dict.items() (busquénlo en google)
 """
 creditos={'MexicoMagico':10,'Algebra':10,'Procesos':10,'Programacion':10,'Estadistica':10}
 for i in creditos.items():
     print(i[0])
         
 """
 2) Cuenta el número de ocurrencias de palabras en el 
siguiente texto dado:
(Deben Utilizar un diccionario para almacenar las palabras
 y el número de veces que aparecieron)



"Este es un texto de prueba, queremos utilizar este texto 
para hacer una prueba de nuestro programa"
"""
Texto='este es un texto de prueba, queremos utilizar este texto para hacer nuestro programa'
texto=Texto.split()
di={}
for i in texto:
    di[i]=0
for i in texto:
    if i in di:
        di[i]+=1
print(di)
print()

"""
3) Muestra los productos con el precio menor o igual al que 
introduce el usuario.
(Debes utilizar un diccionario para almacenar los productos)
"""
productos={'computadora':500,'television':500,'Radio':400,'teclado':500,'mouse':100,'ventilador':200}
precio=float(input("Cuanto dinero tienes?"))
cont=0
for i in productos.items():
    if(i[1]<=precio):
        print("puedes comprar",i[0])
        cont= cont +1
    else:
        cont= cont
if (cont==0):
    print("eres pobre,no te alcanza")