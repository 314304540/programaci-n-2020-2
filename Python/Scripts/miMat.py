#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""
def miRaiz(a, e=0.0001):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez mas parecidos.
    Se considera una áproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    #e = 0.0001
    b = 1
    h = a
    while abs(b - h)>e:
        b = (b + h)/2
        h = a/b
    return b

if __name__== "__main_":
    print(miRaiz(9))
    print(miRaiz(9, 0.1))
    print(miRaiz(9, 0.00001))
    print(miRaiz(9, 0.000001))


def abs1(x):
    #primera versión valor absoluto
    if x == 0:
        return x
    elif x < 0:
        x = -1 * x
        return x
print(abs1(-540))

def abs2(x):
    #segunda version valor absoluto
    resultado = 0
    if x >= 0:
        resultado = x
    else:
        resultado = -x 
        return resultado
print(abs2(-3))

def abs3(x):
    #tercera versión valor absoluto
    resultado = 0
    if x >= 0:
        resultado = 0
    if x <= 0:
        resultado = -x
        return resultado

def mcd (a,b):
    q = a // b
    r = a - b*q
    while r != 0:
        a = b
        b = r
        q = a // b
        r = a - b*q
    return b

if __name__== "__main_":
    resultado = mcd(85,25)
    print(resultado)


def MCD (a,b,*c):
    """
    Calcula el mcd para dos o mas valores
    """
    resultado = mcd(a,b)
    for valor in c:
        resultado = mcd(resultado, valor)
    return resultado

print("mcd(24,36,40)={}".format(MCD(24,36,40)))

def MCDtradicional(a,b,c):
    
    resultado = 0
    return resultado 
if __name__ == "__main__":
    print("mcd(24,36,40)={}".format(MCD(24,36,40)))
    print("mcd(24,36,40)={}".format(MCD(24,36)))
    print("mcd(24,36,40)={}".format(MCD(24,36,40,92,1024)))
    print(mcd(85,25))

def sigULAM(n):
    if n%2 == 0:
        return (n/2)
    else:
        return (3*n + 1)

def ULAM(n):
    """
    

    Parameters
    ----------
    n : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    resultado = sigULAM(n)
    while resultado != 1:
        print(resultado)
        resultado = sigULAM (resultado)
ULAM(45)

#Función suma(n) regresa la suma de los primeros n enteros iniciando por n y terminando con 0    
    
def suma(n):
    summ = 0
    for n in range (1, n+1):
        summ += n
        n -= 1
    print("La suma de los primeros enteros es:", summ)
suma(15)

# Función sumaPares(n) regresa la suma de los pares menores o iguales a n

def sumaPares(n):
    suma = 0
    for n in range(1, n+1):
        if(n % 2) == 0:
            suma += n
        n -= 1
    print("La suma de los pares antes de n es:", suma)
    
sumaPares(7)

#Función sumaImpares(n) regresa la suma de los impares menores o iguales a n

def sumaImpares(n):
    suma = 0
    for n in range(1, n+1):
        if(n % 2) == 1:
            suma += n
        n -= 1
    print("La suma de los impares antes de n es:", suma)

sumaImpares(8)

#Función numeracion(n) muestra la numeración del 1 al n

def numeracion(n):
    for i in range(n):
        print (i + 1)
numeracion(6)

#Función numeracionDecreciente(n) muestra la numeración del n al 1

def numeracionDecreciente(n):
    for i in range(n):
        n -= 1
        print(n+1)
numeracionDecreciente(6)

