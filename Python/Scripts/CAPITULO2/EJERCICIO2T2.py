#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
EJERCICIO2.PY CAPITULO 2

"""
import math
import numpy
#a
a=10**5
print(a)
#b
b=math.sqrt(10)
print(b)
#c

#d
d=math.log(2+math.sqrt(5))
print(d)
#e
e=math.pi*(5.5)**2
print(e)
#f
f=math.sin(2.5)
print(f)
#g
a_g=1
b_g=-7
c_g=10
x1 = (-b_g+math.sqrt(b_g**2-(4*a_g*c_g)))/(2*a_g)
x2 = (-b_g-math.sqrt(b_g**2-(4*a_g*c_g)))/(2*a_g)
#h
h=math.factorial(4)
print(h)
#i ESTA FUNCION ES IMPORTANTE 
k=range(32,129,1)
k=list(k)
print(k)
i=sum(k)
print(i)
#j
j=range(3,18,1)
j=list(j)
print(j)
producto=numpy.prod(j)
print(producto)
