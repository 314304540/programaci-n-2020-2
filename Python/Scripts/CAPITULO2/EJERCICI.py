#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EJERCICIO3.PY

@author: macbookpro
"""
b_num = list(input("Ingresa un numero binario: "))
valor = 0

for i in range(len(b_num)):
	digito = b_num.pop()
	if digito == '1':
		valor = valor + pow(2, i)
print("El numero binario a deciaml es", valor)