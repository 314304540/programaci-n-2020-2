#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 14:02:19 2020

@author: macbookpro
"""
#coseno.py
import math
def coseno(x,n):
    cosx = 1
    sign = -1
    for i in range(2, n, 2):
        pi=22/7
        y=x*(pi/180)
        cosx = cosx + (sign*(y**i))/math.factorial(i)
        sign = -sign
    return cosx
x=int(input("ingresa x en grados"))
n=int(input("ingresa el numero de terminos"))
print(round(coseno(x,n),2))