#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: macbookpro
"""


###IPythonPracticando02_12022020.py###

#1.- Calcular las raíces de x^2 + 3x  - 5  = 10 (el símbolo ^ representa la potencia, por lo que la expresión se lee: x al cuadrado más tres x menos cinco igual a 10 ). Una vez que se tengan los valores de las reíces (x1 y   x2) comprobar el resultado .
a=9
b=a; h=1
b=(b+h)/2; h=a/b; print(b,h)
b=(b+h)/2; h=a/b; print(b,h)
b=(b+h)/2; h=a/b; print(b,h)
b=(b+h)/2; h=a/b; print(b,h)
b=(b+h)/2; h=a/b; print(b,h)
b=(b+h)/2; h=a/b; print(b,h)
#2.- Calcular el área de un círculo de radio 10.5.
r=10.5
pi=3.1416
print("este programa calcula el area de un circulo de radio 10.5")
area=pi*(r**2)
print("El area es:",area)

#3.- Calcular 10 factorial paso a paso.
num = 10
factorial = 1

if num < 0:
   print("El factorial solo vale para numeros positivos")
elif num == 0:
   print("El factorial de 0 es 1")
else:
   for i in range(1,num + 1):
       factorial = factorial*i
   print("El factorial de 10 ",num,"es",factorial)

#4.- Ejercicio 4 página 35 del libro "An Introduction to Python and Computer Programming"
   #a)
 #b)La funcion de monto es M=C(1+i)^t con M=MONTO C=CAPITAL I=INTERES T=PERIODO
   i=0.041
   t=5
   c=10000
   m=c*(1+i)**t
   print("el monto final será",m)
   """entonces si se busca que el monto final sea 50,000 lo que se hace es despejar C para que M=50,000"""
   capital=40899.34536864
   monto=capital*(1+i)**t
   #c)
   #d)3335.8=3000(1+i)^3
   """ se despeja i"""
   mon=3335.8
   cap=3000
   tiem=3
   interes=((mon/cap)**(1/3))-1
   print(interes)
   #e) usando la formula de Heron 
   l1=3
   l2=4
   l3=6
   perimetro=l1+l2+l3
   print(perimetro)
   semiperimetro=perimetro/2
   print(semiperimetro)
   area=(semiperimetro*(semiperimetro-l1)*(semiperimetro-l2)*(semiperimetro-l3))
   print(area)


#5.- Convertir de metros a pies, de kilos a libras, de pesos a dólares, de Kilowatts a Amperes en un circuito de 120 volts, de Fahrenheit a Kelvin y viceversa.
print("este programa es un convertidor de metros a pies")
metros=int(input("ingresa la cantidad a convertir en metros"))
pies=metros*3.28
print(metros,"equivale a",pies,"pies")
print("este programa es un convertidor de kilos a libras")
kilos=int(input("ingresa la cantidad en kilos a convertir a libras"))
libras=kilos*2.20462
print(kilos,"equivale a",libras,"libras")
print("este programa es un convertidor de pesos a dolares")
pesos=int(input("ingresa la cantidad en pesos a convertir"))
dolares=pesos*0.046
print(pesos,"equivale a",dolares,"dolares")
print("este programa convierte killowatts a amperes en un circuito de 120 volts")
V = 120        
R = 8          

I = V/R
print ('I = %0.2f Amps'%I)
print("este programa convierte Fareheits a grados kelvin")
far=int(input("ingresa la cantidad a convertir"))
kelvin=273.5 + ((far - 32.0) * (5.0/9.0))
print(far,"equivale a",kelvin,"grados kelvin")



