#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: macbookpro
"""

#REGEX como se le conoce a las operaciones regulares son secuencias de carcateres que forman un patron de  busqueda
import re

#[] se usan brackets para definir un conjunto de caracteres "permitidos" para el patron que se va a buscar
txt="hola mundo"
x=re.findall("[a-m]",txt)
print(x)

#. implica que puede existir cualquier caracter menos "newline"
#ho..la
txt="hello h##o heiio he wrld"
x=re.findall("he..o",txt)
print(x)

#secuencia especial "\d"
txt="Adrian cumplió 25 años"
x=re.findall("\d",txt)
if x:
    print("la cadena comienza con un hola")
else:
    print("la cadena no comienza con hola")
    
#$ es para especificae con que patron termina la cadena
txt="hola mundo"
x=re.findall("mundo$",txt)
if x:
    print("la cadena termina con mundo")
else:
    print("la cadena no termina con mundo")
    
# + una o mas ocurrencias
strr="Ana compró una manzana y una bolsa"
x=re.findall("an+",strr)
print(x)
if x:
     print("hay al menos un match")
 else:
     print("no hay ningun match")
     
#{} para especificar exactamente el numero de ocurrencias de cierto patron
# al{2}
     
strs=" a Daniel no le gusta la comida salada all"
x=re.findall("al{2}",strs)
print(x)
if x:
    print("hay un match")
else:
    print("no hay ningun match")
    
#| para uno u otro "hola|adios"
txt= "Te gustan las peras o las manzanas?"
x=re.findall("peras|manzanas",txt)
print(x)
if x:
    print("hay al menos un match")
else:
    print("no hay ningun match")
    
