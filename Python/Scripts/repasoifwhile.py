#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: macbookpro
"""

#Este en repasoifWhile

###(1) Ciclo infinito que se detinen en 7. ###
"""
n =int(input("ingresa un numero"))
while n!=7:
    print("El ciclo sigue corriendo")
    if n == 7:
         break
     print(n)
     print('ciclo terminado')
     """
n=int(input("ingresa un numero"))
while n!= 7:
    print("El ciclo sigue corriendo")
    if n==7:
        break
    print("el ciclo termina cuando presionas 7")

###(2)  Verificar de manera indefinda si un numero esta dentro del intervalo o no.###
import random
n=int(input("ingresa el limite inferior del intervalo"))
m=int(input("ingresa el limite superior del intervalo"))

def intervalo(n,m):
    cont=0
    a=random.randint(n,m)
    print(a)
    while True:
        cont+=1
        num=int(input("introduce un numero"))
        if num==a:
            print("El numero esta dentro del intervalo ingresado",cont)
            break
        elif num!=a:
            print("fallaste, intentalo de nuevo")
intervalo(n,m)



###(3)  Verificar n veces si un numero esta dentro del intervalo o no. ###
import random
s=int(input("ingresa el limite inferior del intervalo"))
l=int(input("ingresa el limite superior del intervalo"))

number = random.randint(s, l)

number_of_guesses = 0

while number_of_guesses < 5:
    print('Verifica si el numero está dentro del intervalo')
    guess = input()
    guess = int(guess)

    number_of_guesses = number_of_guesses + 1

    if guess == number:
        break

if guess == number:
    print('Adivinaste el numero en ' + str(number_of_guesses) + ' intentos')

else:
    print('El número no esta en el intervalo')
