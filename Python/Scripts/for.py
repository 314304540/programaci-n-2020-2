#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 13:12:07 2020

@author: macbookpro
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 19:13:37 2020

@author: luis
"""
#utilidades.py debe estar en el directorio Scripts
from utilidades import espera, agregaRutas

agregaRutas(['/Documents/programaci-n-2020-2-/Python/Scripts/', 
             '/Documents/programaci-n-2020-2-/Python/Scripts/26022020/'],
"home")

cadena = "Esta es una cadena no muy larga"
for caracter in cadena:
    print(caracter)
espera()
    
materias = ["Álgebra II", "Cálculo II", "Programación"]
for materia in materias:
    print(materia)
espera()    

carrera = [["Álgebra I", "Cálculo I", "THC"], materias,[]]
for semestre in carrera:
    print(semestre)
espera()
    ##
edades = [17, 18 ,19, 20, 21]
for edad in edades:
    print(edad)
espera()
    
edades.append(40)
for edad in edades:
    print(edad, end=" años ")
print()
espera()
    
tuplaEdades = (17, 18 ,19, 20, 21)

FacultadDeCiencias=[["Actuaría",[["Primer semestre"],
                                 ["Segundo semestre",["Algebra",10, "Cálculo I", 10, "Geometría I", 10]],
                                 ["Tercer semestre"],
                                 ["Cuarto semestre"],
                                 ["Quinto semestre"],
                                 ["Sexto semestre"],
                                 ["Septimo semestre"],
                                 ["Octavo semestre"]
                                 ]],
                    ["Biología",[]],
                    ["Ciencias de la Computación",[]],
                    ["Ciencias de la Tierra",[]],
                    ["Física",[]],
                    ["Física Biomédica",[]],
                    ["Matemáticas",[]],
                    ["Matemáticas aplicadas",[["Primer semestre"],
                                 ["Segundo semestre"],
                                 ["Tercer semestre"],
                                 ["Cuarto semestre"],
                                 ["Quinto semestre"],
                                 ["Sexto semestre"],
                                 ["Septimo semestre"],
                                 ["Octavo semestre"]
                                 ]],
                    ]
UNAM = [["Ciudada Unversitaria",["Facultad de Arquitectura", "Facultad de Ciencias"]],
        ["FES Iztacala",[]],
        ["FES Zaragoza",[]],
        ["FES Acatlan",[]],
        ["SISAL",[]] 
         ]
espera()

try:
    tuplaEdades.append(40)
    tuplaEdades.pop()
except Exception as e:
    print("Error: "+ str(e))
    print("Error: {}".format(e))
print(FacultadDeCiencias[0])