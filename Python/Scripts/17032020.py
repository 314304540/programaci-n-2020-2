#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: macbookpro
"""

#17032020



#(1) Leer numeros desde el teclado hasta que el usuario ingrese 0, finalmente 
#mostrar la suma de los numeros ingresados
num=int(input("ingresa un numero entero"))
if num < 0:
   print("Ingresa un entero positivo")
else:
   sum = 0
  
   while(num > 0):
       sum += num
       num -= 1
   print("la suma es: ", sum)


#(2) Mostrar un menu, de 3 opciones 1-Comenzar programa 2-Imprimir un listado o 
#3-Finalizar programa si ingresa algo didstinto debe mandar un error.
def pedirNumeroEntero():

	correcto=False
	num=0
	while(not correcto):
		try:
			num = int(input("Introduce un numero entero: "))
			correcto=True
		except ValueError:
			print('Error, introduce un numero entero')
	
	return num

salir = False
opcion = 0

while not salir:

	print ("1. Opcion 1")
	print ("2. Opcion 2")
	print ("3. Opcion 3")
	print ("4. Salir")
	
	print ("Elige una opcion")

	opcion = pedirNumeroEntero()

	if opcion == 1:
		print ("Opcion 1")
	elif opcion == 2:
		print ("Opcion 2")
	elif opcion == 3:
		print("Opcion 3")
	elif opcion == 4:
		salir = True
	else:
		print ("Error: debes introducir un numero entre 1 y 3")

print ("Fin")