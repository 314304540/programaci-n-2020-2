#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 13:30:59 2020

@author: macbookpro
"""

#120520
"""
#Escribir una función que reciba como entrada 10 números ingresados por el usuario
#esta función tiene que devolver una lista con el cuadrado de los números 
#que ingresó el usuario, no se puede utilizar ninguna función predefinida en 
#Python para elevar al cuadrado, además de que se debe verificar que cada
#que cada cadena ingresada es un número, de no ser  así mandar un mensaje
#"La cadena ingresada no es número"
# Hint: Utilizar función isnumeric
"""
lista=[]
lista2 = []
for i in range(10):
    a = input("Ingrese un numero \n")
    if a.isnumeric() is True:
        lista.append(int(a))
    else:
        print("La cadena ingresada no es un número")
for i in lista:
    i = i**2
    lista2.append(i)
    
print( lista2)
