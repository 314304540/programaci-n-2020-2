print('''
  Bienvenido al resolvedor de ecuaciones
  En este programa, las ecuaciones tienen la forma
  (a  b)=(c)
  (d  e)=(f)
  Ingrese los valores de los parámetros para resolver la ecuación:

  ''')

'''
 si a=d, entonces (b-e)x=c-f => x=(c-f)/(b-e)
Luego, ay+b(c-f)/(b-e)=c => y=(c-bx)/a
                   
si a!=d, entonces a*(dy+ex)=a*f ady+aex=af
Por otra parte, da+db=dc
'''

a=float(input("Valor de a: "))
b=float(input("Valor de b: "))
c=float(input("Valor de c: "))
d=float(input("Valor de d: "))
e=float(input("Valor de e: "))
f=float(input("Valor de f: "))


"""
En este metodo buscamos hacer cero una variable, por lo que queremos 
sumar o restar para eliminar la x_i, primero hay que comparar si x_1 
son iguales porque de ser asi entonces  podemos restar y despejar c/b=x_2
"""


def despeje(a,b,c,d,e,f):
  x=(c-f)/(b-e)
  y=(c-b*x)/a
  return x,y


         

if a==d and b!=e:
  r1,r2=despeje(a,b,c,d,e,f)
  print('En tu sistema lineal, x es igual a',r1,'y es igual a',r2)

elif a==d and b==e:
	print('El sistema lineal tiene renglones linealmente dependientes, por lo que no se puede resolver')

elif a!=0 and d!=0:
  a_0=a
  d_0=d
  a=a*d_0
  b=b*d_0
  c=c*d_0
  d=d*a_0
  e=e*a_0
  f=f*a_0
  r1,r2=despeje(a,b,c,d,e,f)
  print('En tu sistema lineal, x es igual a',r1,'y es igual a',r2)
else:
  print('Elige otro sistema de ecuaciones')