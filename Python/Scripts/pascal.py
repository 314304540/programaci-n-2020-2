#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on  May 17 16:11:15 2020

@author: macbookpro
"""

#triangulo de pascal
triangulo_pascal = []

def listavacia(n):
    while len(triangulo_pascal) < n:
        triangulo_pascal.append([0])

def Pascal_renglones(renglones):
    listavacia(renglones)
    for element in range(renglones):
        cont = 1
        while cont < renglones - element:
            triangulo_pascal[cont + element].append(0)
            cont += 1
    for renglones in triangulo_pascal:
        renglones.insert(0, 1)
        renglones.append(1)
    triangulo_pascal.insert(0, [1, 1])
    triangulo_pascal.insert(0, [1])

Pascal_renglones(10)

for row in triangulo_pascal:
    print(row)
    
#Entonces los ceros será el lugar que ocupe las sumas de los renglones anteriores
import math as m

def combinaciones(n, r): 
    return int((m.factorial(n)) / ((m.factorial(r)) * m.factorial(n - r)))

def PRUEBA(x, y):
    for y in range(x):
        return combinaciones(x, y)

def Pascal(rows):
    resultado = [] 
    for cont1 in range(rows):
        row = [] 
        for elemento in range(cont1 + 1): 
        
            row.append(combinaciones(cont1, elemento))
        resultado.append(row)
        # count += 1 # avoidable
    return resultado

for row in Pascal(10):
    print(row)